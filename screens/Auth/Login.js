import React, { Component } from 'react';
import { View, Text, KeyboardAvoidingView, StyleSheet, SafeAreaView, StatusBar, TextInput, TouchableOpacity, Alert } from 'react-native';
import { connect } from 'react-redux';

import { currentUser, setInitialRouteName, setUserCart, setHistoryTransaction } from '../../redux/actions';
import PrimaryButton from '../../components/general/PrimaryButton';
import CustomizeTextInput from '../../components/input/CustomizeTextInput';
import Constant from '../../utilities/Constant';

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: ''
        };
    }

    handleChangeText = (id, value) => {
        if (id == "email") {
            this.setState({ email: value })
        }
        else {
            this.setState({ password: value })
        }
    }

    handleRedirectUser(page) {
        this.props.navigation.replace(page)
    }

    handleOnSubmitPressed() {
        const { email, password } = this.state;
        if (email == "admin@gmail.com" && password == "1234567890") {
            this.props.dispatch(currentUser(this.state));
            this.props.dispatch(setInitialRouteName("Main"));
            this.props.dispatch(setUserCart([]));
            this.props.dispatch(setHistoryTransaction([]));
            this.handleRedirectUser("Main")
        }
        else {
            Alert.alert('Warning', "please fill email with admin@gmail.com and password with 1234567890")
        }
    }

    render() {
        return (
            <View style={localStyles.container}>
                <Text style={localStyles.title}>Selamat Datang</Text>
                <Text style={localStyles.label}>Silahkan masuk untuk melanjutkan</Text>

                <CustomizeTextInput
                    id="email"
                    isActive={true}
                    placeholder="Email"
                    value={this.state.email}
                    onChangeText={this.handleChangeText}
                />
                <CustomizeTextInput
                    id="password"
                    isActive={true}
                    placeholder="Password"
                    value={this.state.password}
                    onChangeText={this.handleChangeText}
                    isSecure={true}
                />
                <PrimaryButton
                    type="active"
                    text="Submit"
                    onPress={() => this.handleOnSubmitPressed()}
                />
            </View>
        );
    }
}

const localStyles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        padding: 16,
        backgroundColor: Constant.COLOR.ABSOLUTE_WHITE
    },
    title: {
        fontWeight: 'bold',
        fontSize: 24,
        color: Constant.COLOR.LABEL,
        marginBottom: 8
    },
    label: {
        fontSize: 16,
        color: Constant.COLOR.LABEL,
        marginBottom: 8
    }
})

const mapStateToProps = (state) => ({
    user: state.user,
    initialRouteName: state.initialRouteName,
    cart: state.cart,
    history: state.history
})

//make this component available to the app
export default connect(mapStateToProps)(Login);