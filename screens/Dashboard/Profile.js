import React, { Component } from 'react';
import { View, Text, StyleSheet, SafeAreaView, StatusBar, TouchableOpacity } from 'react-native';
import Constant from '../../utilities/Constant';

export default class Profile extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    handleOnMenuPressed(page){
        this.props.navigation.navigate(page)
    }

    render() {
        return (
            <SafeAreaView style={localStyles.container}>
                <StatusBar
                    backgroundColor={Constant.COLOR.ABSOLUTE_WHITE}
                    barStyle="dark-content"
                />
                <Text style={localStyles.title}>Welcome to your wallet </Text>
                <TouchableOpacity
                    style={localStyles.menu}
                    onPress={() => this.handleOnMenuPressed('CryptoWallet')}
                >
                    <Text style={localStyles.label}>My Wallet</Text>
                </TouchableOpacity>
                <TouchableOpacity
                    style={localStyles.menu}
                    onPress={() => this.handleOnMenuPressed('HistoryTransaction')}
                >
                    <Text style={localStyles.label}>History Transaction</Text>
                </TouchableOpacity>
            </SafeAreaView>
        );
    }
}

const localStyles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Constant.COLOR.ABSOLUTE_WHITE,
    },
    title: {
        fontSize: 24,
        color: Constant.COLOR.LABEL,
        fontWeight: 'bold',
        margin: 16
    },
    label: {
        fontSize: 16,
        color: Constant.COLOR.LABEL,
    },
    menu: {
        borderBottomWidth: 0.5,
        padding: 16,
    }
})
