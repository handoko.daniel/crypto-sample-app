import React, { Component } from 'react';
import { View, Text, SafeAreaView, StatusBar, StyleSheet, FlatList } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';

// import custom component
import StaticHeader from '../../components/header/StaticHeader';
import ApiServices from '../../services/ApiServices';
import Constant from '../../utilities/Constant';
import ApiRoutes from '../../services/ApiRoutes';
import CryptoCard from '../Crypto/page_components/CryptoCard';

export default class Dashboard extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: []
        };
    }

    componentDidMount() {
        this.handleGetHomepage()
    }
    async handleGetHomepage() {
        try {
            let userToken = Constant.USER_TOKEN;
            ApiServices.defaults.headers.common = {
                "X-CMC_PRO_API_KEY": userToken,
            };
            //Calling Get Coin Market API from ApiServices.js
            ApiServices.get(ApiRoutes.ApiGetHomepage)
                .then(async (response) => {
                    var APIresult = response.data;
                    this.setState({ data: APIresult.data })
                })
                .catch
                (
                    async (error) => {
                        if (error == 'Error: Request failed with status code 401') {
                            console.log("error 401 failed to Coin Market Data: ", error);
                        }
                        else if (error == 'Error: Request failed with status code 422') {
                            console.log("error 422 failed to Coin Market Data: ", error);
                        }
                        else {
                            console.log("error 500 Coin Market Data: ", error.response);
                        }
                    }
                );

        }
        catch (error) {
            console.log("error: ", error);
        }
    }

    handleRedirectUser(page, data) {
        this.props.navigation.navigate(page, { data: data })
    }

    renderCrypto() {
        const { data } = this.state;
        if (data.length > 0) {
            return (
                <FlatList
                    data={data}
                    keyExtractor={item => item.id}
                    renderItem={({ item, index, separators }) => (
                        <CryptoCard
                            data={item}
                            onPress={() => this.handleRedirectUser('CryptoDetail', item)}
                        />
                    )}

                />
            )
        }
    }

    render() {
        return (
            <SafeAreaView style={localStyles.container}>
                <StatusBar
                    barStyle="dark-content"
                    backgroundColor={Constant.COLOR.ABSOLUTE_WHITE}
                />
                <StaticHeader
                    type="default"
                />
                <View style={localStyles.tableHeader}>
                    <View style={{ flex: 1 }}>
                        <Text style={localStyles.label}>Name</Text>
                    </View>
                    <View style={{ flex: 1 }}>
                        <Text style={localStyles.label}>Market Cap</Text>
                    </View>
                    <View style={{ flex: 1 }}>
                        <Text style={localStyles.label}>1h Change</Text>
                    </View>
                </View>
                {this.renderCrypto()}
            </SafeAreaView>
        );
    }
}

const localStyles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Constant.COLOR.ABSOLUTE_WHITE
    },
    tableHeader: {
        width: "100%",
        borderBottomWidth: 0.5,
        borderColor: Constant.COLOR.THIN_GRAY,
        padding: 16,
        flexDirection: 'row'
    },
    label: {
        color: Constant.COLOR.LABEL,
        fontWeight: 'bold'
    }
})
