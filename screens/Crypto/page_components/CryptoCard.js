import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';

import Constant from '../../../utilities/Constant';

export default class CryptoCard extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    renderMarketChange() {
        const { data } = this.props;
        if (data.quote.USD.percent_change_1h > 0) {
            return (
                <Text style={localStyles.positiveLabel}>{this.props.data.quote.USD.percent_change_1h} %</Text>
            );
        }
        else {
            return (
                <Text style={localStyles.negativeLabel}>{this.props.data.quote.USD.percent_change_1h} %</Text>
            );
        }
    }

    render() {
        console.log(this.props.data);
        return (
            <TouchableOpacity
                onPress={this.props.onPress}
                style={localStyles.container}
            >
                <View style={{ flex: 1 }}>
                    <Text style={localStyles.label}>{this.props.data.name} [{this.props.data.symbol}]</Text>
                </View>
                <View style={{ flex: 1 }}>
                    <Text style={localStyles.label}>{this.props.data.quote.USD.market_cap}</Text>
                </View>
                <View style={{ flex: 1 }}>
                    {this.renderMarketChange()}
                </View>
            </TouchableOpacity>
        );
    }
}

const localStyles = StyleSheet.create({
    container: {
        width: "100%",
        borderBottomWidth: 0.5,
        borderColor: Constant.COLOR.THIN_GRAY,
        padding: 16,
        flexDirection: 'row'
    },
    label: {
        color: Constant.COLOR.LABEL
    },
    positiveLabel: {
        color: Constant.COLOR.SUCCESS
    },
    negativeLabel: {
        color: Constant.COLOR.DANGER
    }
})