import React, { Component } from 'react';
import { View, Text, StyleSheet, SafeAreaView, StatusBar, Dimensions, TouchableOpacity } from 'react-native';
import { LineChart } from "react-native-chart-kit";
import { connect } from 'react-redux';

import { setUserCart, setHistoryTransaction } from '../../redux/actions';
import PrimaryButton from '../../components/general/PrimaryButton';
import StaticHeader from '../../components/header/StaticHeader';
import Constant from '../../utilities/Constant';

class CryptoDetail extends Component {
    constructor(props) {
        super(props);
        const navigationParam = this.props.route.params.data;
        this.state = {
            data: navigationParam
        };
    }

    handleRedirectUser(page) {
        if (page == "Back") {
            this.props.navigation.goBack();
        }
    }

    handleOnBuyPressed() {
        const { cart, history } = this.props;
        const { data } = this.state;
        let historyTransaction = history.slice()
        let cryptoObject = {
            "quantity": 1,
            "name": this.state.data.name,
            "symbol": this.state.data.symbol
        }
        let cryptoObjectHistory = {
            "quantity": 1,
            "name": this.state.data.name,
            "symbol": this.state.data.symbol
        }
        if (cart.length < 0) {
            cart.push(cryptoObject);
            this.props.dispatch(setUserCart(cart));
        }
        else {
            let duplicateProductIndex = -1;
            for (let index = 0; index < cart.length; index++) {
                if (cart[index].name == data.name) {
                    duplicateProductIndex = index;
                }
            }

            if (duplicateProductIndex >= 0) {
                cart[duplicateProductIndex].quantity = parseInt(cart[duplicateProductIndex].quantity) + 1;
            }
            else {
                cart.push(cryptoObject);
            }
            this.props.dispatch(setUserCart(cart));
        }
        
        historyTransaction.push(cryptoObjectHistory)
        this.props.dispatch(setHistoryTransaction(historyTransaction));
        this.props.navigation.goBack();
    }

    renderChart() {
        const { data } = this.state;
        return (
            <LineChart
                data={{
                    labels: ["1h", "24h", "30d", "60d", "7d", "90d"],
                    datasets: [
                        {
                            data: [
                                data.quote.USD.percent_change_1h,
                                data.quote.USD.percent_change_24h,
                                data.quote.USD.percent_change_30d,
                                data.quote.USD.percent_change_60d,
                                data.quote.USD.percent_change_7d,
                                data.quote.USD.percent_change_90d,
                            ]
                        }
                    ]
                }}
                width={Dimensions.get("window").width - 64} // from react-native
                height={200}
                // yAxisLabel="Changes "
                // yAxisSuffix=" rb"
                yAxisInterval={4} // optional, defaults to 1
                chartConfig={{
                    backgroundColor: Constant.COLOR.ABSOLUTE_WHITE,
                    backgroundGradientFrom: Constant.COLOR.ABSOLUTE_WHITE,
                    backgroundGradientTo: Constant.COLOR.ABSOLUTE_WHITE,
                    decimalPlaces: 0, // optional, defaults to 2dp
                    color: (opacity = 1) => `rgba(82, 121, 111, ${opacity})`,
                    labelColor: (opacity = 1) => `rgba(82, 121, 111, ${opacity})`,
                    style: {
                        borderRadius: 16,
                    },
                    propsForDots: {
                        r: "2",
                        strokeWidth: "0.5",
                        stroke: Constant.COLOR.PRIMARY
                    }
                }}
                bezier
                style={{
                    marginVertical: 8,
                    borderRadius: 16,
                    marginTop: 16,
                    marginLeft: 16,
                    marginBottom: 16
                }}
            />
        )
    }

    render() {
        return (
            <SafeAreaView style={localStyles.container}>
                <StatusBar
                    barStyle="dark-content"
                    backgroundColor={Constant.COLOR.ABSOLUTE_WHITE}
                />

                <StaticHeader
                    type="back-button"
                    onGoBack={() => this.handleRedirectUser('Back')}
                />
                <View style={localStyles.contentContainer}>
                    {this.renderChart()}
                    <Text style={localStyles.title}>{this.state.data.name} [{this.state.data.symbol}]</Text>
                    <Text style={localStyles.thinLabel}>$ {this.state.data.quote.USD.price}</Text>
                    <PrimaryButton
                        type="active"
                        text="Buy"
                        onPress={() => this.handleOnBuyPressed()}
                    />
                </View>
            </SafeAreaView>
        );
    }
}

const localStyles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Constant.COLOR.ABSOLUTE_WHITE,
    },
    contentContainer: {
        padding: 16
    },
    title: {
        fontWeight: 'bold',
        fontSize: 24,
        marginBottom: 4,
        color: Constant.COLOR.LABEL
    },
    thinLabel: {
        color: Constant.COLOR.THIN_GRAY,
        fontSize: 16,
        marginBottom: 4
    }
})


const mapStateToProps = (state) => ({
    user: state.user,
    initialRouteName: state.initialRouteName,
    cart: state.cart,
    history: state.history
})

//make this component available to the app
export default connect(mapStateToProps)(CryptoDetail);