import React, { Component } from 'react';
import { View, Text, SafeAreaView, StatusBar, StyleSheet, ScrollView } from 'react-native';
import { connect } from 'react-redux';
import StaticHeader from '../../components/header/StaticHeader';

import { setUserCart } from '../../redux/actions';
import Constant from '../../utilities/Constant';

class CryptoWallet extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    handleRedirectUser(page) {
        if (page == "Back") {
            this.props.navigation.goBack();
        }
    }

    renderCard() {
        const { cart } = this.props;
        if (cart.length > 0) {
            return (
                cart.map((item, index) =>
                    <View
                        key={index}
                        style={localStyles.cardContainer}
                    >
                        <View style={{ flex: 1 }}>
                            <Text>{item.name}</Text>
                        </View>
                        <View style={{ flex: 1 }}>
                            <Text>{item.quantity}</Text>
                        </View>
                    </View>
                )
            )
        }
        else {
            return (<View />)
        }
    }

    render() {
        return (
            <SafeAreaView style={localStyles.container}>
                <StatusBar
                    barStyle="dark-content"
                    backgroundColor={Constant.COLOR.ABSOLUTE_WHITE}
                />

                <StaticHeader
                    type="back-button"
                    onGoBack={() => this.handleRedirectUser('Back')}
                />
                <View
                    style={localStyles.cardContainer}
                >
                    <View style={{ flex: 1 }}>
                        <Text style={{ fontWeight: 'bold', fontSize: 16 }}>Name</Text>
                    </View>
                    <View style={{ flex: 1 }}>
                        <Text style={{ fontWeight: 'bold', fontSize: 16 }}>Quantity</Text>
                    </View>
                </View>
                <ScrollView style={localStyles.contentContainer}>
                    {this.renderCard()}
                </ScrollView>
            </SafeAreaView>
        );
    }
}

const localStyles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Constant.COLOR.ABSOLUTE_WHITE
    },
    cardContainer: {
        width: "100%",
        flexDirection: 'row',
        padding: 16,
        borderBottomWidth: 0.5
    }
})

const mapStateToProps = (state) => ({
    cart: state.cart
})

//make this component available to the app
export default connect(mapStateToProps)(CryptoWallet);
