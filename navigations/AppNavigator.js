import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import Login from '../screens/Auth/Login';
import CryptoDetail from '../screens/Crypto/CryptoDetail';
import CryptoWallet from '../screens/Crypto/CryptoWallet';
import HistoryTransaction from '../screens/Crypto/HistoryTransaction';
import Main from './TabNavigator';

const Stack = createStackNavigator();

function App() {
    return (
        <NavigationContainer>
            <Stack.Navigator>
                <Stack.Screen
                    name="Login" component={Login}
                    options={{ headerShown: false }}
                />
                <Stack.Screen
                    name="Main" component={Main}
                    options={{ headerShown: false }}
                />
                <Stack.Screen
                    name="CryptoDetail" component={CryptoDetail}
                    options={{ headerShown: false }}
                />
                <Stack.Screen
                    name="CryptoWallet" component={CryptoWallet}
                    options={{ headerShown: false }}
                />
                <Stack.Screen
                    name="HistoryTransaction" component={HistoryTransaction}
                    options={{ headerShown: false }}
                />
            </Stack.Navigator>
        </NavigationContainer>
    );
}

export default App;