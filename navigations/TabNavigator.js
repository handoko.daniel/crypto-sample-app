import * as React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Icon from 'react-native-vector-icons/Ionicons';

import Dashboard from '../screens/Dashboard/Dashboard';
import Profile from '../screens/Dashboard/Profile';
import Constant from '../utilities/Constant';

const Tab = createBottomTabNavigator();

export default function App() {
    return (
        <Tab.Navigator
            screenOptions={({ route }) => ({
                tabBarIcon: ({ focused, color, size }) => {
                    let iconName;

                    if (route.name === 'Home') {
                        iconName = focused
                            ? 'ios-home'
                            : 'ios-home-outline';
                    } else if (route.name === 'Profile') {
                        iconName = focused ? 'ios-person' : 'ios-person-outline';
                    }

                    // You can return any component that you like here!
                    return <Icon name={iconName} size={size} color={color} />;
                },
            })}
            tabBarOptions={{
                showLabel: false,
                activeTintColor: Constant.COLOR.PRIMARY,
                inactiveTintColor: Constant.COLOR.THIN_GRAY,
                style: { height: 64 }
            }}
        >
            <Tab.Screen name="Home" component={Dashboard} />
            <Tab.Screen name="Profile" component={Profile} />
        </Tab.Navigator>
    );
}