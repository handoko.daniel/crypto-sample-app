import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

import Constant from '../../utilities/Constant';

export default class StaticHeader extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    renderType() {
        const { type } = this.props;

        if (type == "default") {
            return (
                <View style={localStyles.container}>
                    <Text style={localStyles.title}>Crypto Apps</Text>
                </View>
            )
        }
        else if (type == "back-button") {
            return (
                <View style={localStyles.container}>
                    <TouchableOpacity
                        onPress={this.props.onGoBack}
                    >
                        <Icon
                            name="ios-chevron-back"
                            size={24}
                        />
                    </TouchableOpacity>
                    <Text style={localStyles.title}>Back</Text>
                </View>
            )
        }
        else {
            return (<View />)
        }
    }

    render() {
        return (
            this.renderType()
        );
    }
}

const localStyles = StyleSheet.create({
    container: {
        height: 64,
        alignItems: 'center',
        paddingHorizontal: 16,
        borderBottomWidth: 0.5,
        borderColor: Constant.COLOR.THIN_GRAY,
        flexDirection: 'row'
    },
    title: {
        fontWeight: 'bold',
        fontSize: 16,
        color: Constant.COLOR.LABEL
    }
})