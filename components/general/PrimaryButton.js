import React, { Component } from 'react';
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native';
import Constant from '../../utilities/Constant';

export default class PrimaryButton extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    renderButtonType() {
        const { type } = this.props;
        if (type == "active") {
            return (
                <TouchableOpacity
                    onPress={this.props.onPress}
                    style={localStyles.activeContainer}
                >
                    <Text style={[localStyles.label, localStyles.activeLabel]}>{this.props.text}</Text>
                </TouchableOpacity>
            )
        }
        else {
            return (
                <View
                    style={localStyles.disableContainer}
                >
                    <Text style={localStyles.label}>{this.props.text}</Text>
                </View>
            )
        }
    }

    render() {
        return (
            this.renderButtonType()
        );
    }
}

const localStyles = StyleSheet.create({
    activeContainer: {
        marginTop: 16,
        width: "100%",
        padding: 16,
        backgroundColor: Constant.COLOR.PRIMARY,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 16,
    },
    disableContainer: {
        marginTop: 16,
        justifyContent: 'center',
        alignItems: 'center',
        width: "100%",
        padding: 16,
        backgroundColor: Constant.COLOR.THIN_GRAY,
        borderRadius: 16
    },
    label: {
        fontWeight: 'bold',
        fontSize: 16,
        color: Constant.COLOR.LABEL
    },
    activeLabel: {
        color: Constant.COLOR.ABSOLUTE_WHITE
    }
})