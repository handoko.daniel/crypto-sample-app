import axios from 'axios';

//setting configuration API for Axios
export const BASE_URL_PRODUCTION = 'https://pro-api.coinmarketcap.com/v1';

//setting base url that will be used in apps
export const BASE_URL = BASE_URL_PRODUCTION;

//setting API timeout in ms
export const API_TIMEOUT = 30000;

class ApiServices {
    constructor() {
        this.instance = undefined;
    }

    static createInstance() {
        this.instance = axios.create({
            baseURL: BASE_URL,
            timeout: API_TIMEOUT,
            headers: { 'Content-Type': 'application/json' }
        });
    }

    static getInstance() {
        if (this.instance === undefined) {
            this.createInstance();
        }

        return this.instance;
    }
}

export const MultipartInstance = axios.create({
    baseURL: BASE_URL,
    timeout: API_TIMEOUT,
    headers: {
        'Content-Type': 'multipart/form-data',
        'Accept': 'application/json'
    }
})

export default ApiServices.getInstance();