export default {
    USER_TOKEN: '37b24df0-3994-4954-8195-f4f6b7c44318',
    COLOR: {
        PRIMARY: '#12664F',
        ABSOLUTE_WHITE: '#FFFFFF',
        LABEL: '#454545',
        THIN_GRAY: '#C1C1C1',
        
        SUCCESS: '#4FB477',
        WARNING: '#E6AF2E',
        DANGER: '#BA1F33'
    }
}