export const CURRENT_USER = 'CURRENT_USER';
export const INITIAL_ROUTE_NAME = 'INITIAL_ROUTE_NAME';
export const USER_CART = 'USER_CART';
export const HISTORY_TRANSACTION = 'HISTORY_TRANSACTION'

export function currentUser(payload) {
    return function (dispatch) {
        dispatch({
            type: CURRENT_USER,
            payload
        });
    }
}
export function setInitialRouteName(payload) {
    return function (dispatch) {
        dispatch({
            type: INITIAL_ROUTE_NAME,
            payload
        });
    }
}
export function setUserCart(payload) {
    return function (dispatch) {
        dispatch({
            type: USER_CART,
            payload
        });
    }
}
export function setHistoryTransaction(payload) {
    return function (dispatch) {
        dispatch({
            type: HISTORY_TRANSACTION,
            payload
        });
    }
}
