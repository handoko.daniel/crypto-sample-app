import {
    CURRENT_USER,
    INITIAL_ROUTE_NAME,
    USER_CART,
    HISTORY_TRANSACTION
} from './actions';

const initialState = {
    user: null,
    initialRouteName: "Introduction",
    cart: [],
    history: []
}

export default (state = initialState, action) => {
    const { type, payload } = action;

    switch (type) {
        case CURRENT_USER:
            return {
                ...state,
                user: payload
            };
        case INITIAL_ROUTE_NAME:
            return {
                ...state,
                initialRouteName: payload
            }
        case USER_CART:
            return {
                ...state,
                cart: payload
            }
        case HISTORY_TRANSACTION:
            return {
                ...state,
                history: payload
            }
        default:
            return state;
    }
};
